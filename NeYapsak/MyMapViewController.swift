//
//  ViewController.swift
//  NeYapsak
//
//  Created by Erkan Ugurlu on 27/10/15.
//  Copyright © 2015 Erkan Ugurlu. All rights reserved.
//

import UIKit
import MapKit
import SafariServices
import GoogleMobileAds

enum CustomError: Error {
    case serviceError
}

class MyMapViewController: UIViewController {

    var clickedEvent: Int {
        get{
            return UserDefaults.standard.integer(forKey: "EventCounter")
        } set {
            UserDefaults.standard.set(newValue, forKey: "EventCounter")
        }
    }
    
    var locationManager: CLLocationManager!
    var userAnnotation: MKAnnotation!
    var events:[Event] = []
    var currentDateString: String!
    var lastDateString: String!
    
    @IBOutlet var mapview:MKMapView!
    @IBOutlet var activityView:UIActivityIndicatorView!
    
    var interstitial: GADInterstitial!
    var selectedUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapview.showsUserLocation = false;
        self.mapview.delegate = self;
        
        self.locationManager = CLLocationManager()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        
        self.lastDateString = ""
        currentDateString = Date().formattedString

        self.refreshContent()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MyMapViewController.refreshContent), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
        
        self.interstitial = createAndLoadInterstitial()

    }
    
    @objc func refreshContent(){
        self.locationManager.startUpdatingLocation()
        guard  currentDateString != lastDateString else {
            return
        }
        lastDateString = currentDateString
        self.callService()
    }
    
    func getEventTypeIcon(paramString:String?) -> UIImage {
        var imagename = "triskelion";
        if let paramString = paramString {
            let str = paramString.lowercased().trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            if str  == "tiyatro"  {
                imagename = "theater"
            } else if str == "atölye kurs" || str == "kurs"{
                imagename = "library"
            } else if str == "konser parti" || str == "konser" {
                imagename = "dancinghall";
            } else if str == "sergi"{
                imagename = "aboriginal";
            } else if str == "yarışma" {
                imagename = "waterpark";
            }
        }

        return UIImage(named: imagename)!
    }

    func callService() {
        self.activityView.startAnimating()
        self.mapview.removeAnnotations(self.events.annotations)
        self.events.removeAll()
        URLSession.shared.dataTask(with: URL(string: "http://neyapsak.ervanux.xyz/events")!,
                                   completionHandler: { (data, response, error) -> Void in
                                    self.handleData(data: data, error: error)
        }).resume()
    }

    func handleData(data:Data?, error:Error?){
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                self.showErrorPopup(message: error?.localizedDescription ?? "Connection Error")
                self.activityView.stopAnimating()
            }
            return
        }

        do {
            var events = try JSONDecoder().decode([Event].self, from: data)
            guard events.count > 0 else {
                throw CustomError.serviceError
            }
            
            if userAnnotation != nil {
                events.sort { (first, second) -> Bool in
                    first.annotationPresentation!.coordinate.distance(from: userAnnotation.coordinate) < second.annotationPresentation!.coordinate.distance(from: userAnnotation.coordinate)
                }
            }
            
            currentDateString = Date().formattedString
            self.events = events
            DispatchQueue.main.async {
                self.mapview.addAnnotations(self.events.annotations)
                self.mapview.showAnnotations(self.events.annotations, animated: true)
                
                if self.userAnnotation != nil {
                    if let tenthAnnotation =  events[(events.count > 10 ? 10 : (events.count - 1))].annotationPresentation {
                        let distanceToTenth = self.userAnnotation.coordinate.distance(from: tenthAnnotation.coordinate)
                        
                        //                    let rect = MKMapRect(origin: MKMapPoint(self.userAnnotation.coordinate), size: MKMapSize(width: distanceToTenth, height: distanceToTenth))
                        self.mapview.setRegion(MKCoordinateRegion(center: self.userAnnotation.coordinate, latitudinalMeters: distanceToTenth, longitudinalMeters: distanceToTenth), animated: true)
                    }
                }
                self.activityView.stopAnimating()
            }
        } catch {
            DispatchQueue.main.async {
                self.activityView.stopAnimating()
                self.showErrorPopup(message: "Content Error")
            }
            return
        }

    }

    func handleResponse(message:[String]?) {
        if let result = message?.first {
            self.handleData(data: Data(base64Encoded: result), error: nil)
        } else {
            self.handleData(data: nil, error: CustomError.serviceError)
        }
    }

    func zoomToLocation(zoomLocation:CLLocationCoordinate2D) {
        let  viewRegion = MKCoordinateRegion(center: zoomLocation,latitudinalMeters: 20000,longitudinalMeters: 20000);
        self.mapview.setRegion(viewRegion,animated:true)
    }
    
}

extension MyMapViewController {
    
    @IBAction func pressedZoomOut(sender:AnyObject) {
        var region = self.mapview.region;
        var span = MKCoordinateSpan()
        span.latitudeDelta = min(region.span.latitudeDelta*2, 180)
        span.longitudeDelta = min(region.span.longitudeDelta*2, 180)
        region.span = span;
        
        self.mapview.setRegion(region,animated:true)
    }
    
    @IBAction func pressedZoomIn(sender:AnyObject) {
        var region = self.mapview.region;
        var span = MKCoordinateSpan()
        span.latitudeDelta = region.span.latitudeDelta/2;
        span.longitudeDelta = region.span.longitudeDelta/2;
        region.span = span;
        self.mapview.setRegion(region,animated:true)
    }
    
    @IBAction func pressedMyLocation(sender:AnyObject) {
        locationManager.startUpdatingLocation()
    }
}

extension UIViewController{
    
    func showErrorPopup(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertAction.Style.cancel, handler: {(action) -> Void in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension MyMapViewController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
let reuseIdentifier = "CustomPinAnnotationView"
extension MyMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let evnt = annotation as? EventAnnotation else {
            return nil
        }
        
        if let event = self.events.first(where: {$0.annotationPresentation == evnt}) {
            var pinView = mapview.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
            if pinView == nil {
                pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
                pinView?.canShowCallout = true
                pinView?.rightCalloutAccessoryView = UIButton(type: .infoLight)
            } else {
                pinView?.annotation = evnt
            }
            pinView?.image = self.getEventTypeIcon(paramString: event.type)
            
            return pinView
        } else {
            var pinView = mapview.dequeueReusableAnnotationView(withIdentifier: "UserCustomPinAnnotationView")
            if pinView == nil {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "UserCustomPinAnnotationView")
                pinView?.canShowCallout = true
                pinView?.image = UIImage(named: "me")
            }
            return pinView
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //        self.performSegue(withIdentifier: "ShowWebView", sender: view.annotation)
        if let annotation = view.annotation as? EventAnnotation,
            let event = self.events.first(where: { $0.annotationPresentation == annotation }),
            let urlString = event.link.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let detailUrl = URL(string: urlString) {
            print("\(detailUrl)")
            self.clickedEvent += 1
//            print("Ad status: \(interstitial.isReady)")
            if self.clickedEvent % globalConfig.adShowFrequency == 0 && interstitial.isReady {
                self.selectedUrl = detailUrl
                interstitial.present(fromRootViewController: self)
            } else {
                let safariVC = SFSafariViewController(url: detailUrl)
                self.present(safariVC, animated: true, completion: nil)
                safariVC.delegate = self
            }
        }
    }
}

extension MyMapViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last?.coordinate else {
            return
        }
        self.zoomToLocation(zoomLocation: location)
        locationManager.stopUpdatingLocation()
        
        if self.userAnnotation != nil {
            self.mapview.removeAnnotation(userAnnotation)
            self.userAnnotation=nil;
        }
        self.userAnnotation = EventAnnotation(coordinate: location, title: "", subtitle: nil)
        self.mapview.addAnnotation(self.userAnnotation)
    }
}

extension Date {
    
    var formattedString: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "ddMMYYYY"
        return dateFormat.string(from: Date())
    }
}

extension MyMapViewController {
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: globalConfig.GadUnitID)
        interstitial.delegate = self
        let request = GADRequest()
        request.testDevices = ["89b7c320b77995916e0f7501822dcc52"]
        interstitial.load(request)
        return interstitial
    }

}


extension MyMapViewController : GADInterstitialDelegate {
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
//        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
//        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialDidDismissScreen")
        if selectedUrl != nil {
            let safariVC = SFSafariViewController(url: selectedUrl!)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        //Refresh ad
        self.interstitial = createAndLoadInterstitial()
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
//        print("interstitialWillLeaveApplication")
    }
}

//
//  Types.swift
//  NeYapsak
//
//  Created by e on 30.09.18.
//  Copyright © 2018 Erkan Ugurlu. All rights reserved.
//

import Foundation
import MapKit

class Event: Codable {
    let title: String
    let location: String
    let type: String?
    let link: String
    let coordinate: Coordinate?
    
    lazy var annotationPresentation: EventAnnotation? = {
        guard let coordinate = coordinate else {
            return nil
        }
        return EventAnnotation(coordinate: coordinate.getCLLocationCoordinate2D(), title: title, subtitle: location)
    }()
}

extension Array where Element == Event {
    var annotations:[EventAnnotation] {
        return self.compactMap{ $0.annotationPresentation }
    }
}

class EventAnnotation:NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
}

struct Coordinate: Codable {
    let lat: Double
    let lng: Double
    
    func getCLLocationCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
}

extension CLLocationCoordinate2D {
    //distance in meters, as explained in CLLoactionDistance definition
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination = CLLocation(latitude:from.latitude,longitude:from.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }
}

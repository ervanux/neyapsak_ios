//
//  AppDelegate.swift
//  NeYapsak
//
//  Created by Erkan Ugurlu on 27/10/15.
//  Copyright © 2015 Erkan Ugurlu. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseMessaging

class Config {
    var remoteConfig = RemoteConfig.remoteConfig()
    var GadApplicationID : String {
        //TEST
        return "ca-app-pub-9324324427002484~5747382533"
    }
    
    var GadUnitID: String {
        #if DEBUG
            return "ca-app-pub-3940256099942544/4411468910"
        #else
            return "ca-app-pub-9324324427002484/9618288815"
        #endif
    }
    
    var adShowFrequency: Int {
        var ad_show_frequency = remoteConfig.configValue(forKey: "ad_show_frequency").numberValue?.intValue
        if ad_show_frequency == nil || ad_show_frequency == 0 {
            ad_show_frequency = 3
        }
        
        return ad_show_frequency!
        
    }
    
    var askForPushPermission: Bool {
        return remoteConfig.configValue(forKey: "ask_push_permission").boolValue
    }
}

let globalConfig = Config()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        FirebaseApp.configure()
        GADMobileAds.configure(withApplicationID: globalConfig.GadApplicationID)
        globalConfig.remoteConfig.fetch(withExpirationDuration: 10) { (status, error) in
            if error == nil {
                globalConfig.remoteConfig.activateFetched()
            }
            
            DispatchQueue.main.async {
                if globalConfig.askForPushPermission {
                    Messaging.messaging().delegate = self
                    self.registerApn(application: application)
                }
            }
        }
        
        
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    
    func registerApn(application:UIApplication)  {
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()

    }
}
